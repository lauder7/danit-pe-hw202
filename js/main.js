let sUserName = "";
let sUserAge = "";
let nUserAge;
let bAllowed = false;

lbMain: {
    do {
        sUserName = prompt("Enter your name", sUserName);
        if (sUserName === null) break lbMain;
    } while (!sUserName);

    do {
        sUserAge = prompt("Enter your age", sUserAge);
        if (sUserAge === null) break lbMain;
        nUserAge = +sUserAge;
    } while (!sUserAge || Number.isNaN(nUserAge));

    if (nUserAge < 18) break lbMain;
    if (nUserAge <= 22 && !confirm("Are you sure you want to continue?")) break lbMain;

    bAllowed = true;
}


if (bAllowed)
    alert(`Welcome, ${sUserName}!`);
else
    alert("You are not allowed to visit this website");

